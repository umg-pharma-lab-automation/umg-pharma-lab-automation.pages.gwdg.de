# UMG Pharma Lab Automation
Find the landing page [here](https://umg-pharma-lab-automation.pages.gwdg.de).

If you are a group member, the [wiki](https://gitlab.gwdg.de/umg-pharma-lab-automation/umg-pharma-lab-automation.pages.gwdg.de/-/wikis/home) contains local copies of documentation files, as well as other additional information.
